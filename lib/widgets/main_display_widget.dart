import 'package:expenses_control_app/models/transaction.dart';
import 'package:flutter/material.dart';

import 'chart.dart';
import 'transaction_list.dart';

class MainDisplayWidget extends StatefulWidget {
  final Function _deleteTransaction;
  final List<Transaction> _userTransactions;
  final PreferredSizeWidget _appBar;

  MainDisplayWidget(
      this._appBar, this._userTransactions, this._deleteTransaction);

  @override
  _MainDisplayWidgetState createState() =>
      _MainDisplayWidgetState(_appBar, _userTransactions, _deleteTransaction);
}

class _MainDisplayWidgetState extends State<MainDisplayWidget> {
  final List<Transaction> _userTransactions;
  final PreferredSizeWidget _appBar;
  final Function _deleteTransaction;

  MediaQueryData _mediaQuery;
  bool _showChart = false;

  _MainDisplayWidgetState(
      this._appBar, this._userTransactions, this._deleteTransaction);

  @override
  Widget build(BuildContext context) {
    _mediaQuery = MediaQuery.of(context);
    final _isLandscape = _mediaQuery.orientation == Orientation.landscape;

    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          if (_isLandscape) ..._buildLandsCapeContent(),
          if (!_isLandscape) ..._buildPortraitContent(context, _appBar, 0.3)
        ]);
  }

  List<Widget> _buildLandsCapeContent() {
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Show Chart",
            style: Theme.of(context).textTheme.headline6,
          ),
          Switch.adaptive(
            activeColor: Theme.of(context).accentColor,
            value: _showChart,
            onChanged: (val) {
              setState(() {
                _showChart = val;
              });
            },
          )
        ],
      ),
      _showChart
          ? _buildPortraitContent(context, _appBar, 0.7)
          : showTransactionList(context, _appBar, 0.7),
    ];
  }

  List<Widget> _buildPortraitContent(
      BuildContext context, PreferredSizeWidget appBar, double height) {
    return [
      Container(
        height: _getCalculatedHeight(context, appBar, height),
        child: Chart(_recentTransactions),
      ),
      showTransactionList(context, _appBar, 0.7),
    ];
  }

  double _getCalculatedHeight(
      BuildContext context, PreferredSizeWidget appBar, double heightPct) {
    return (_mediaQuery.size.height -
            appBar.preferredSize.height -
            _mediaQuery.padding.top) *
        heightPct;
  }

  List<Transaction> get _recentTransactions {
    return _userTransactions
        .where((tx) => tx.date.isAfter(
              DateTime.now().subtract(Duration(days: 7)),
            ))
        .toList();
  }

  Container showTransactionList(
      BuildContext context, PreferredSizeWidget appBar, double height) {
    return Container(
      height: _getCalculatedHeight(context, appBar, height),
      child: TransactionList(_userTransactions, _deleteTransaction),
    );
  }
}
